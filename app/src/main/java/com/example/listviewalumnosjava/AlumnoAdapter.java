package com.example.listviewalumnosjava;


import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.bumptech.glide.Glide;


public class AlumnoAdapter extends RecyclerView.Adapter<AlumnoAdapter.ViewHolder> implements Filterable, View.OnClickListener {
    private ArrayList<AlumnoItem> listaAlumnos;
    private ArrayList<AlumnoItem> original;
    private View.OnClickListener listener;
    private Application context;
    private LayoutInflater inflater;

    public AlumnoAdapter(ArrayList<AlumnoItem> data, Application context) {
        this.listaAlumnos = new ArrayList<>();
        this.listaAlumnos.addAll(data);
        this.context = context;
        this.original = data;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public void onClick(View v) {
        if(listener != null) {
            listener.onClick(v);
        }
    }

    @NonNull
    @NotNull
    @Override
    public AlumnoAdapter.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.alumnos_items, null);
        view.setOnClickListener(this);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AlumnoAdapter.ViewHolder holder, int position) {
        AlumnoItem alumno = original.get(position);
        holder.txtMatricula.setText(alumno.getMatricula());
        holder.txtNombre.setText(alumno.getNombre());
        holder.txtCarrera.setText(alumno.getCarrera());

        if(alumno.getImageID() == null) {
            holder.idImagen.setImageResource(R.drawable.usuario);
        }
        else{
            Bitmap bitmap = BitmapFactory.decodeByteArray(alumno.getImageID(), 0, alumno.getImageID().length);
            Glide.with(context)
                    .load(bitmap)
                    .into(holder.idImagen);

        }

    }

    @Override
    public long getItemId(int posicion) {
        return posicion;
    }

    @Override
    public int getItemCount() {
        return original.size();
    }

    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    public View getView(int posicion, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if(convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.alumnos_items, parent, false);

            holder = new ViewHolder(convertView);
            holder.idImagen = convertView.findViewById(R.id.foto);
            holder.txtMatricula = convertView.findViewById(R.id.txtMatricula);
            holder.txtNombre = convertView.findViewById(R.id.txtNombre);
            holder.txtCarrera = convertView.findViewById(R.id.txtCarrera);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        AlumnoItem item = listaAlumnos.get(posicion);

        Bitmap bitmap = BitmapFactory.decodeByteArray(item.getImageID(), 0, item.getImageID().length);
        Glide.with(context)
                .load(bitmap)
                .into(holder.idImagen);
        holder.txtMatricula.setText(item.getNombre());
        holder.txtNombre.setText(item.getMatricula());
        holder.txtCarrera.setText((item.getCarrera()));

        return convertView;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtNombre;
        private TextView txtMatricula;
        private TextView txtCarrera;
        private ImageView idImagen;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtNombre = itemView.findViewById(R.id.txtAlumnoNombre);
            txtMatricula = itemView.findViewById(R.id.txtMatricula);
            txtCarrera = itemView.findViewById(R.id.txtCarrera);
            idImagen = itemView.findViewById(R.id.foto);
        }

        /*public void bind(AlumnoItem alumno) {
            txtNombre.setText(alumno.getNombre());
            txtMatricula.setText(alumno.getMatricula());
            txtCarrera.setText(alumno.getCarrera());

            if (alumno.getImageID() != null) {
                Bitmap bitmap = BitmapFactory.decodeByteArray(alumno.getImageID(), 0, alumno.getImageID().length);
                Glide.with(context)
                        .load(bitmap)
                        .into(idImagen);
            } else {

            }
        }*/
    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                List<AlumnoItem> filteredList = new ArrayList<>();

                if (constraint == null || constraint.length() == 0) {
                    filteredList.addAll(listaAlumnos);
                } else {
                    String filterPattern = constraint.toString().toLowerCase().trim();
                    for (AlumnoItem item : listaAlumnos) {
                        if (item.getMatricula().toLowerCase().contains(filterPattern) || item.getNombre().toLowerCase().contains(filterPattern)) {
                            filteredList.add(item);
                        }
                    }
                }

                results.values = filteredList;
                results.count = filteredList.size();
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                original.clear();
                original.addAll((ArrayList<AlumnoItem>) results.values);
                notifyDataSetChanged();
            }
        };
    }

}