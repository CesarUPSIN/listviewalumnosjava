package com.example.listviewalumnosjava;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import Modelo.AlumnosDb;

public class AlumnoAlta extends AppCompatActivity {
    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final int REQUEST_IMAGE_GALLERY = 2;

    private Button btnFoto, btnGuardar, btnBorrar, btnRegresar;
    private AlumnoItem alumno;
    private EditText txtNombre, txtMatricula, txtGrado;
    private ImageView imgAlumno;
    private TextView lblImagen;
    private String carrera = "Ing. Tec. Información";
    private int posicion;
    private int ID = 0;

    Uri selectedImageUri;

    private AlumnosDb alumnosDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_alumno_alta);
        btnFoto = (Button) findViewById(R.id.btnFoto);
        btnGuardar = (Button) findViewById(R.id.btnSalir);
        btnBorrar = (Button) findViewById(R.id.btnBorrar);
        btnRegresar = (Button) findViewById(R.id.btnRegresar);
        txtMatricula = (EditText) findViewById(R.id.txtMatricula);
        txtGrado = (EditText) findViewById(R.id.txtGrado);
        txtNombre = (EditText) findViewById(R.id.txtNombre);
        imgAlumno = (ImageView) findViewById(R.id.imgAlumno);
        lblImagen = (TextView) findViewById(R.id.lblFoto);

        Bundle bundle = getIntent().getExtras();
        alumno = (AlumnoItem) bundle.getSerializable("alumno");
        posicion = bundle.getInt("posicion");
        ID = bundle.getInt("id");

        if(posicion >= 0) {
            txtMatricula.setText(alumno.getMatricula());
            txtNombre.setText(alumno.getNombre());
            txtGrado.setText(alumno.getCarrera());
            byte[] alumnoFoto = alumno.getImageID();

            if(alumnoFoto != null){
                Bitmap bitmap = BitmapFactory.decodeByteArray(alumnoFoto, 0, alumnoFoto.length);
                Glide.with(getApplicationContext())
                        .load(bitmap)
                        .into(imgAlumno);
            }

        }

        //btnFoto = findViewById(R.id.btnFoto);
        btnFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                seleccionarImagen();
            }
        });

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            //byte[] foto = obtenerDatosImagen();
            @Override
            public void onClick(View v) {
                if(alumno == null) {
                    alumno = new AlumnoItem();
                    alumno.setCarrera(txtGrado.getText().toString());
                    alumno.setMatricula(txtMatricula.getText().toString());
                    alumno.setNombre(txtNombre.getText().toString());
                    byte[] foto = obtenerDatosImagen();
                    alumno.setImageID(foto);

                    if(validar()) {
                        alumnosDb = new AlumnosDb(getApplicationContext());
                        Aplicacion.alumnos.add(alumno);
                        alumnosDb.insertAlumno(alumno);
                        setResult(Activity.RESULT_OK);
                        finish();
                    }
                    else {
                        Toast.makeText(getApplicationContext(), " Falto capturar datos", Toast.LENGTH_SHORT).show();
                        txtMatricula.requestFocus();
                    }
                }

                if(posicion >= 0) {
                    alumno.setMatricula(txtMatricula.getText().toString());
                    alumno.setNombre(txtNombre.getText().toString());
                    alumno.setCarrera(txtGrado.getText().toString());
                    byte[] foto = obtenerDatosImagen();
                    alumno.setImageID(foto);

                    // Actualiza la tabla de alumnos
                    Aplicacion.alumnosDb.updateAlumno(alumno);

                    Aplicacion.alumnos.get(posicion).setId(alumno.getId());
                    Aplicacion.alumnos.get(posicion).setMatricula(alumno.getMatricula());
                    Aplicacion.alumnos.get(posicion).setNombre(alumno.getNombre());
                    Aplicacion.alumnos.get(posicion).setCarrera(alumno.getCarrera());
                    Aplicacion.alumnos.get(posicion).setImageID(alumno.getImageID());

                    Toast.makeText(getApplicationContext(), " Se modificó con exito", Toast.LENGTH_SHORT).show();
                    setResult(Activity.RESULT_OK);
                    finish();
                }
            }
        });

        btnBorrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (posicion >= 0) {

                    AlertDialog.Builder mensaje = new AlertDialog.Builder(AlumnoAlta.this);
                    mensaje.setTitle("Alumno");
                    mensaje.setMessage("¿Desea eliminarlo?");
                    mensaje.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Log.d("ID", "ID: " + alumno.getId());
                            Aplicacion.alumnosDb.deleteAlumno(ID);
                            Aplicacion.alumnos.remove(posicion);
                            Aplicacion.adaptador.notifyItemRemoved(posicion);
                            Toast.makeText(getApplicationContext(), "Se ha eliminado ", Toast.LENGTH_SHORT).show();
                            setResult(Activity.RESULT_OK);
                            finish();
                            Aplicacion.adaptador.notifyDataSetChanged();
                        }
                    });

                    mensaje.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

                    mensaje.show();
                }
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });
    }

    public boolean validar() {
        boolean exito = true;
        Log.d("nombre", "validar: " + txtNombre.getText());
        if(txtNombre.getText().toString().equals("") || txtMatricula.getText().toString().equals("") || txtGrado.getText().toString().equals("")) exito = false;

        return exito;
    }

    private byte[] obtenerDatosImagen() {
        byte[] datosImagen = null;

        try {
            Bitmap bitmap = ((BitmapDrawable) imgAlumno.getDrawable()).getBitmap();

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            datosImagen = stream.toByteArray();
            stream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return datosImagen;
    }



    public void seleccionarImagen() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/.png");
        startActivityForResult(intent, REQUEST_IMAGE_GALLERY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_IMAGE_GALLERY && resultCode == RESULT_OK && data != null && data.getData() != null) {
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
                this.imgAlumno.setImageBitmap(bitmap);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

}