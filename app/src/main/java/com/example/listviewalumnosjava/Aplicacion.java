package com.example.listviewalumnosjava;

import android.app.Application;
import android.util.Log;

import java.util.ArrayList;

import Modelo.AlumnosDb;

public class Aplicacion extends Application {
    public static ArrayList<AlumnoItem> alumnos;
    public static AlumnoAdapter adaptador;

    public ArrayList<AlumnoItem> getAlumnos() {
        return alumnos;
    }

    public AlumnoAdapter getAdaptador() {
        return adaptador;
    }
    static AlumnosDb alumnosDb;

    @Override
    public void onCreate() {
        super.onCreate();
        alumnosDb = new AlumnosDb(getApplicationContext());
        this.alumnos = alumnosDb.allAlumnos();
        alumnosDb.openDataBase();

        this.adaptador = new AlumnoAdapter(this.alumnos, this);
        Log.d("", "onCreate: tamaño array list " + alumnos.size());
    }
}
