package com.example.listviewalumnosjava;

import android.net.Uri;

import java.io.Serializable;
import java.util.ArrayList;

public class AlumnoItem implements Serializable {
    private int id;
    private String carrera;
    private String nombre;
    private String matricula;
    private byte[] imageID;

    public AlumnoItem() {
        this.id = 0;
        this.nombre = "";
        this.carrera = "";
        this.matricula = "";
        this.imageID = null;
    }
    public AlumnoItem(String carrera, String nombre, String matricula, byte[] imageID) {
        this.carrera = carrera;
        this.nombre = nombre;
        this.matricula = matricula;
        this.imageID = imageID;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setImageID(byte[] imageID) {
        this.imageID = imageID;
    }

    public byte[] getImageID() {
        return imageID;
    }
}
